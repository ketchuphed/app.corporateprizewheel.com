<?php
require_once "../vendor/autoload.php";
require_once "../routes/load.php";

Flight::set('flight.views.path', '../templates');

Flight::start();